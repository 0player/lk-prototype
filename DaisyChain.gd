extends Node

signal setup_complete(viewport)

var first_pass := preload("res://ViewportToLuminanceRenderPass.tscn")
var intermediate_pass := preload("res://ExposureRenderPass.tscn")

export(int, 0, 10) var passes: int = 5
export var debug: bool = true

var glast: ViewportTexture

# Called when the node enters the scene tree for the first time.
func _ready():
	var ibuf_size : int = 1
	for _i in range(passes):
		ibuf_size *= 3
		
	var tbuf_size = float(ibuf_size)
	
	var fp_instance = first_pass.instance()
	add_child(fp_instance)
	fp_instance.set_source_viewport_and_size(get_viewport(), Vector2(ibuf_size, ibuf_size))
	
	var last: Viewport = fp_instance
	
	var chain_left = 0
	
	if debug:
		var s = Sprite.new()
		add_child(s)
		s.position.x = chain_left
		s.texture = last.get_texture()
		s.centered = false
		s.scale = Vector2(.1, .1)
		chain_left += tbuf_size * 0.1 + 10
	
	while ibuf_size > 1:
		ibuf_size /= 3
		var ip_instance := intermediate_pass.instance()
		add_child(ip_instance)
		var size := ibuf_size
		if size == 1:
			size = 2
		ip_instance.set_source_viewport_and_size(last, Vector2(size, size))
		last = ip_instance
		if debug:
			var s = Sprite.new()
			add_child(s)
			s.position.x = chain_left
			s.texture = last.get_texture()
			s.centered = false
			s.scale = Vector2(0.1 * tbuf_size / ibuf_size, 0.1 * tbuf_size / ibuf_size)
			chain_left += tbuf_size * 0.1 + 10
	
	yield(VisualServer, "frame_post_draw")
	last.set_screen_blend()
	emit_signal("setup_complete", last)
	glast = last.get_texture()

func _unhandled_key_input(_event):
	if !glast: return
	var img = glast.get_data()
	print(img.get_size())
	img.lock()
	var px :Color = img.get_pixel(0, 0)
	var l: float = px.r + px.g * 10 + px.b * 100
	print("the luminance is: ", l, " ", px);
	img.unlock()
