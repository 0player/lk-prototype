extends Viewport

onready var sm: ShaderMaterial = $Node2D.material

func set_source_viewport_and_size(source: Viewport, set_size: Vector2):
	size = set_size
	$Node2D.texture = source.get_texture()
	print("size is: ", $Node2D.rect_size)

func set_screen_blend():
	usage = Viewport.USAGE_2D
	sm = sm.duplicate()
	$Node2D.material = sm
	sm.set_shader_param("screen_blend", true)
	var im := get_texture().get_data()
	im.lock()
	print("at begin, data = ", im.get_pixel(0, 0));
	im.unlock()
	sm.set_shader_param("previousPass", get_texture())
	sm.set_shader_param("delta_time", 0.0)
	set_process(true)

func _ready():
	set_process(false)

func _process(delta):
	sm.set_shader_param("delta_time", delta)
