extends Viewport

onready var rect = $Node2D
onready var sm: ShaderMaterial = $Node2D.material

func set_source_viewport_and_size(source: Viewport, set_size: Vector2):
	size = set_size
	print("source size is: ", source.size)
	sm.set_shader_param("SOURCE_SIZE", PoolIntArray([source.size.x, source.size.y]));
	sm.set_shader_param("TARGET_SIZE", PoolIntArray([set_size.x, set_size.y]));
	rect.texture = source.get_texture()
	print("rect size is: ", rect.rect_size)
	print("rect is: ", rect.rect_position)
