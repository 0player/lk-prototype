extends HSlider

signal value_committed


func _gui_input(event):
	if event is InputEventMouse:
		$SliderTooltip.show()
		$SliderTooltip.margin_left = event.position.x + 5
		$SliderTooltip.margin_top = margin_bottom + 5
		if event is InputEventMouseButton:
			if event.button_index == BUTTON_LEFT and event.pressed == false:
				emit_signal("value_committed", value)
				accept_event()

func set_tooltip(val):
	$SliderTooltip.text = val
