shader_type canvas_item;
render_mode unshaded, blend_disabled;
uniform ivec2 SOURCE_SIZE;
uniform ivec2 TARGET_SIZE;

// Converts a color from sRGB gamma to linear light gamma
vec3 toLinear(vec3 sRGB)
{
    bvec3 cutoff = lessThan(sRGB, vec3(0.04045));
    vec3 higher = pow((sRGB + vec3(0.055))/vec3(1.055), vec3(2.4));
    vec3 lower = sRGB/vec3(12.92);

    return mix(higher, lower, cutoff);
}

void fragment() {
	vec2 fc = FRAGCOORD.xy;
	ivec2 pixel_coord = ivec2(fc) * SOURCE_SIZE / TARGET_SIZE;
	ivec2 next_coord = ivec2(fc + vec2(1)) * SOURCE_SIZE / TARGET_SIZE;
	float lumi = 0.0;
	for (int i = pixel_coord.x; i < next_coord.x; ++i) {
		for (int j = pixel_coord.y; j < next_coord.y; ++j) {
			vec4 color = texelFetch(TEXTURE, ivec2(i, j), 0);
			lumi += dot(toLinear(color.rgb), vec3(0.2126, 0.7152, 0.0722));
		}
	}
	lumi /= float((next_coord.y - pixel_coord.y) * (next_coord.x - pixel_coord.x));
	COLOR = vec4(min(1.0, lumi), clamp((lumi - 1.0) / 100.0, 0.0, 1.0), max(0.0, (lumi - 11.0) / 100.0), 1); //vec4((lumi-1.0)/10.0, lumi, TIME, 1);
}