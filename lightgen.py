# -*- coding: utf-8 -*-
"""
Created on Sat Jun  6 19:38:23 2020

@author: alex0
"""
#%%
BASE_SIZE = 2048
UPSCALE = 2
QUADRANT_SIZE = 48

from PIL import Image
from scipy import ndimage
import numpy as np

def forward_gamma(u):
    return (323 * u) / 25 if u <= 0.0031308 else 1.055 * u ** (1/2.4) - 0.055

def backward_gamma(u):
    return 25 * u / 323 if u <= 0.04045 else ((u + 0.055) / 1.055) ** (2.4)

def forward_gamma_vectorized(u):
    return np.where(u <= 0.0031308, (323 * u) / 25, 1.055 * u ** (1/2.4) - 0.055)

def valid_correlate(image, kernel):
    corr = ndimage.correlate(image, kernel, mode='constant', origin=[-(s//2) for s in kernel.shape])
    slices = [slice(i - k + 1) for i, k in zip(image.shape, kernel.shape)]
    return corr[tuple(slices)]

least_representable_colour = backward_gamma(1)

preconvolve_size = (BASE_SIZE + QUADRANT_SIZE) * UPSCALE - 1
center = (preconvolve_size - 1) / 2
kernel_size = QUADRANT_SIZE * UPSCALE

idxrange = np.arange(-center, center + 1)
xx, yy = np.meshgrid(idxrange, idxrange, sparse=True, copy=False)
distance_matrix = np.hypot(xx, yy)

preconvolve = 1 / np.sqrt(distance_matrix)

kernel_size = QUADRANT_SIZE * UPSCALE
kernel = np.ones((kernel_size, kernel_size), dtype=bool)
convoluted = valid_correlate(preconvolve, kernel)
print('convolve done')
#%%
alpha = valid_correlate((distance_matrix > center).astype(int), kernel) == 0

maxvalue = convoluted[(~np.isinf(convoluted)).nonzero()].max()
print('maxvalue is', maxvalue)
convoluted /= maxvalue
np.clip(convoluted, 0, 1, convoluted)
print('gamma transform done')

image = Image.fromarray(np.stack([np.round(convoluted * 255).astype(np.byte),
                                  alpha * np.byte(255)], axis=2), 'LA')
    
image.save("lightmap-{}-{}-x{}-linear.png".format(BASE_SIZE, QUADRANT_SIZE, UPSCALE))



