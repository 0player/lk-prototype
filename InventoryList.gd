extends VBoxContainer

export(Dictionary) var inventory_contents = {} setget _inv_set
export(Array, Resource) var inventory_array = []

const ItemControl = preload("res://InventoryItemControl.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	make_children()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func make_children():
	for child in get_children():
		child.disconnect("value_selected", self, "_on_value_selected")
		child.queue_free()
	for inv_name in inventory_contents:
		var row = ItemControl.instance()
		row.label_amount = inventory_contents[inv_name] as String
		row.label_name = inv_name
		row.label_extra = ""
		row.min_value = 0
		row.max_value = inventory_contents[inv_name]
		row.connect("value_selected", self, "_on_value_selected", [inv_name], CONNECT_PERSIST)
		add_child(row)

func _inv_set(v):
	inventory_contents = v
	if is_inside_tree():
		make_children()

func _on_value_selected(name, value):
	print("selected ", value, " of ", name)
