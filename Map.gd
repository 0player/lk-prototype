extends TileMap

class_name Map

class ManhattanAStar2D:
	extends AStar2D
	
	func _compute_cost(u: int, v: int):
		var a := get_point_position(u)
		var b := get_point_position(v)
		return abs(a.x - b.x) + abs(a.y - b.y)

	func _estimate_cost(u, v):
		return _compute_cost(u, v)

var astar := ManhattanAStar2D.new()

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var x_low: int
var y_low: int
var x_size: int
var y_size: int

# Called when the node enters the scene tree for the first time.
func _ready():
	var rect := get_used_rect()
	x_low = rect.position.x
	y_low = rect.position.y
	x_size = rect.size.x
	y_size = rect.size.y
	for x in range(x_low, x_low + x_size):
		for y in range(y_low, y_low + y_size):
			var id := astar_id(x, y)
			var cellid := get_cell(x, y)
			if cellid != INVALID_CELL and tile_set.tile_get_shape_count(cellid) == 0:
				var world_coord = map_to_world_centered(Vector2(x, y))
				astar.add_point(id, world_coord)
				var nei1 := astar_id(x - 1, y)
				if nei1 > 0 and astar.has_point(nei1):
					astar.connect_points(nei1, id)
				var nei2 := astar_id(x, y - 1)
				if nei2 > 0 and astar.has_point(nei2):
					astar.connect_points(nei2, id)
	print(astar.get_points())
	
	for child in self.get_children():
		if child is StaticBody2D:
			var tile := world_to_map(child.position)
			var id := astar_id(tile.x, tile.y)
			if astar.has_point(id):
				print(id, " disabled")
				astar.set_point_disabled(id)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func get_path_between(p1: Vector2, p2: Vector2) -> PoolVector2Array:
	var id1 := astar.get_closest_point(p1)
	var id2 := astar.get_closest_point(p2)
	var p2_snap := astar.get_point_position(id2)
	if (p2 - p2_snap).length() > cell_size.x:
		return PoolVector2Array([])
	return astar.get_point_path(id1, id2)
	

func astar_id(x: int, y: int) -> int:
	return (x - x_low) * y_size + (y - y_low) + 1
	
func map_to_world_centered(v: Vector2) -> Vector2:
	return .map_to_world(v) + cell_size / 2
