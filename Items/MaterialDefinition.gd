extends Resource

class_name MaterialDefinition
export(float) var solid_density = 1.0
export(Curve) var gas_boundary = null
export(int) var solid_critical_temperature = 273;

export(float, 0, 1000) var gas_boundary_low_temp = 0;
export(float, 0, 1000) var gas_boundary_high_temp = 1000;
