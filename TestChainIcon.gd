extends Sprite

func _ready():
	set_process(false)
	
func _unhandled_key_input(event : InputEventKey):
	if event.pressed and event.scancode == KEY_ENTER:
		self.modulate = self.modulate * 1.5
		print("-------------------------------------")
		set_process(true)

func _process(delta):
	position.x -= 10.0 * delta

func _on_Node2D_setup_complete(viewport):
	(material as ShaderMaterial).set_shader_param("exposure", viewport.get_texture())
