extends MarginContainer

onready var Display = $Display
onready var Slider = $Slider

signal value_selected
signal fastttake
signal contextmenu

export(int) var min_value = 0 setget _set_min
export(int) var max_value = 6 setget _set_max

export(String) var label_amount = "" setget _set_amount
export(String) var label_name = "" setget _set_name
export(String) var label_extra = "" setget _set_extra

export(bool) var support_drag = true

var conversion: FuncRef = funcref(self, '_default_to_string')

func _ready():
	Slider.min_value = min_value
	Slider.max_value = max_value
	$Display/Amount.text = label_amount
	$Display/Name.text = label_name
	$Display/Extra.text = label_extra

func _default_to_string(x):
	return x as String

func _on_Display_gui_input(event):
	if event is InputEventMouseMotion:
		if event.button_mask & BUTTON_MASK_LEFT and support_drag:
			accept_event()
			Display.hide()
			Slider.show()
			var synthetic = InputEventMouseButton.new()
			synthetic.pressed = true
			synthetic.button_index = BUTTON_LEFT
			synthetic.position = event.position
			synthetic.global_position = event.global_position
			Input.parse_input_event(synthetic)
	elif event is InputEventMouseButton:
		if event.doubleclick and event.button == BUTTON_LEFT:
			emit_signal("fasttake")
			accept_event()
		elif event.button_index == BUTTON_RIGHT:
			emit_signal("contextmenu", event.position)
			accept_event()
	else:
		print("event is", event)
	accept_event()


func _on_commit(value):
	emit_signal("value_selected", value)
	Slider.hide()
	Display.show()


func _on_Slider_value_changed(value):
	Slider.set_tooltip(conversion.call_func(value))
	
func _set_min(v):
	if Slider: Slider.min_value = v
	min_value = v

func _set_max(v):
	if Slider: Slider.max_value = v
	max_value = v
	
func _set_amount(v):
	if Display: $Display/Amount.text = v
	label_amount = v
	
func _set_name(v):
	if Display: $Display/Name.text = v
	label_name = v
	
func _set_extra(v):
	if Display: $Display/Extra.text = v
	label_extra = v
