tool
extends "res://IncandescentLight.gd"

func _process(delta):
	if !Engine.editor_hint and temperature < 1400:
		self.temperature += 200 * delta
