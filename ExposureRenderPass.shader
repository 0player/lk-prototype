shader_type canvas_item;
render_mode unshaded, blend_disabled;
uniform sampler2D previousPass : hint_black;
uniform bool screen_blend;
uniform float delta_time;

vec3 lumi_to_rgb(float lumi) {
	return vec3(min(1.0, lumi), clamp((lumi - 1.0) / 10.0, 0.0, 1.0), max(0.0, (lumi - 11.0) / 100.0));
}

float rgb_to_lumi(vec3 rgb) {
	return rgb.r + rgb.g * 10.0 + rgb.b * 100.0;
}

void fragment() {
	ivec2 pixel_coord = ivec2(FRAGCOORD.xy) * 3;
	int total = 0;
	float lumi = 0.0;
	for (int i = 0; i <= 2; ++i) {
		for (int j = 0; j <= 2; ++j) {
			lumi += rgb_to_lumi(texelFetch(TEXTURE, pixel_coord + ivec2(i, j), 0).rgb);
		}
	}
	lumi /= 9.0;
	if (screen_blend && delta_time != 0.0) {
		vec4 screen_color = texelFetch(previousPass, ivec2(FRAGCOORD.xy), 0);
		if (screen_color.a > 0.0) {
			float old_lumi = screen_color.r + screen_color.g * 10.0 + screen_color.b * 100.0;
			highp float target_lumi = pow(lumi, 1.0 - exp(-delta_time * 2.0)) * old_lumi;
			
			lumi = clamp(target_lumi, 0.1, 100.0);
		}
	}
	COLOR = vec4(lumi_to_rgb(lumi), 1.0);
}