shader_type canvas_item;
uniform sampler2D exposure : hint_black;
uniform float moodiness = 1.0;

// Converts a color from linear light gamma to sRGB gamma
vec3 fromLinear(vec3 linearRGB)
{
    bvec3 cutoff = lessThan(linearRGB, vec3(0.0031308));
    vec3 higher = vec3(1.055)*pow(linearRGB, vec3(1.0/2.4)) - vec3(0.055);
    vec3 lower = linearRGB * vec3(12.92);

    return mix(higher, lower, cutoff);
}

// Converts a color from sRGB gamma to linear light gamma
vec3 toLinear(vec3 sRGB)
{
    bvec3 cutoff = lessThan(sRGB, vec3(0.04045));
    vec3 higher = pow((sRGB + vec3(0.055))/vec3(1.055), vec3(2.4));
    vec3 lower = sRGB/vec3(12.92);

    return mix(higher, lower, cutoff);
}
// 

void fragment() {
	vec4 exposure_d = texelFetch(exposure, ivec2(0), 0);
	float lumi_corr = exposure_d.r + exposure_d.g * 10.0 + exposure_d.b * 100.0;
	lumi_corr = mix(lumi_corr, 1.0, float(lumi_corr == 0.0));
	vec4 texcolor = texture(TEXTURE, UV);
	vec4 modulate = mix(MODULATE, vec4(1.0), bvec4(AT_LIGHT_PASS));
	COLOR = vec4(fromLinear(toLinear(texcolor.rgb) * modulate.rgb / moodiness / lumi_corr), texcolor.a * modulate.a);
}


void light() {
	LIGHT = vec4(fromLinear(LIGHT_COLOR.rgb * toLinear(COLOR.rgb)), LIGHT_COLOR.a * COLOR.a);
}