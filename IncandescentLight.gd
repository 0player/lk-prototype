tool
extends Light2D

const TEMP_GRADIENT := preload("res://ColorRamp-800-12200-linear.tres")
const T_LOW := 800.0
const T_HIGH := 12200.0
export(float, 800, 12200) var temperature := 3000.0 setget set_temperature
const T_BASE := 3000
onready var _cache_energy : float = energy
onready var _cache_temp : float = temperature


func set_temperature(v: float):
	var vold := temperature
	if Engine.editor_hint and vold != T_BASE or !Engine.editor_hint and is_inside_tree():
		energy = clamp((float(v) / _cache_temp) * _cache_energy, 0, 4)
	if Engine.editor_hint and vold == T_BASE:
		_cache_temp = v
		_cache_energy = energy
	# TODO make the inspector values update somehow
	temperature = v
	_update_temp()

func _update_temp():
	if temperature >= T_LOW and temperature <= T_HIGH:
		var scale := float(temperature - T_LOW) / (T_HIGH - T_LOW)
		color = TEMP_GRADIENT.interpolate(scale)
