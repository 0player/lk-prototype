extends Node

signal substance_amount_changed(substance_name, new_amount)
signal temperature_changed()

var contents: Dictionary = {}
export(float) var temperature setget set_temperature, get_temperature

var reactions: Array = []
var reaction_speeds: PoolRealArray = []
var reaction_leftovers: PoolRealArray = []

var randomizer := RandomNumberGenerator.new()

func _ready():
	randomizer.seed(get_parent().name.hash())

func _physics_process(delta: float) -> void:
	var components: PoolStringArray = []
	var component_speeds: PoolRealArray = []
	var component_leftovers: PoolRealArray = []
	var component_contents: PoolIntArray = []
	var component_bound: PoolIntArray = []
	var component_seen: Dictionary = {}
	
	for i in range(reactions.size()):
		var r = reactions[i]
		var speed := reaction_speeds[i]
		var leftover := reaction_leftovers[i]
		for j in range(r.components.size()):
			var c = r.components[j]
			var p : int = r.proportions[j]
			if r.proportions[j] >= 0:
				continue
			if component_seen.has(c):
				var idx: int = component_seen[c]
				component_speeds[idx] -= p * speed
				component_leftovers[idx] -= p * leftover
			else:
				component_seen[c] = components.size()
				components.append(c)
				component_speeds.append(-p * speed)
				component_leftovers.append(-p * leftover)
				component_contents.append(contents.get(c, 0))
				component_bound.append(0)
	
	while delta > 0:
		var bound_delta := delta
		var bounding := -1
		# 2020-06-13 as Dan rightfully points out,
		# times that interest
		
		for i in range(component_speeds.size()):
			if bound_delta * component_speeds[i] + component_leftovers[i] < -component_contents[i]:
				# Leftover here may be greater than the component present by itself.
				# However, that is probably fine: that'll set delta to 0,
				# and the reaction won't have a chance to run.
				
				bound_delta = max(0, -(component_contents[i] + component_leftovers[i])/component_speeds[i])
				bounding = i
				if bound_delta == 0:
					component_bound[i] = 1
		component_bound[bounding] = 1
		
		for i in range(component_speeds.size()):
			component_speeds[i] = 0.0
			component_leftovers[i] = 0.0
		
		for i in range(reactions.size()):
			var r = reactions[i]
			var speed := reaction_speeds[i]
			var leftover := reaction_leftovers[i]
			var units := speed * bound_delta + leftover
			var ui := int(units)
			leftover = units - ui
			reaction_leftovers[i] = leftover
			for j in range(r.components.size()):
				var c = r.components[j]
				var p : int = r.proportions[j]
				var idx : int = component_seen[c]
				if p < 0 and component_bound[idx] > 0:
					speed = 0.0
				component_contents[idx] += p * ui
			reaction_speeds[i] = speed
		
			for j in range(r.components.size()):
				var c = r.components[j]
				var p: int = r.proportions[j]
				var idx: int = component_seen[c]
				if p < 0:
					component_speeds[idx] -= p * speed
					component_leftovers[idx] -= p * leftover
		
		delta -= bound_delta
		
	for i in range(component_seen.size()):
		contents[component_seen[i]] = component_contents[i]
		emit_signal("substance_amount_changed", component_seen[i], component_contents[i])

	_recalc_reactions()

func set_temperature(v: float) -> void:
	temperature = v
	call_deferred("_recalc_reactions")

func get_temperature() -> float:
	return temperature
	
func _random_round(v: float) -> int:
	var float_part := v - floor(v)
	if randomizer.randf() < float_part:
		return int(floor(v)) + 1
	return int(floor(v))

func _recalc_reactions() -> void:
	var i := 0
	
	# disable/remove present reactions
	while i < reactions.size():
		var r = reactions[i]
		var speed: float = r.calculate_rate(contents, temperature)
		if speed == 0.0 and reaction_speeds[i] == 0.0:
			# oops, time to go.
			reactions.remove(i)
			reaction_speeds.remove(i)
			reaction_leftovers.remove(i)
		else:
			reaction_speeds[i] = speed
			++i
	
	var new_reactions: Array = _query_database()
	for r in new_reactions:
		if !reactions.has(r):
			var speed: float = r.calculate_rate(contents, temperature)
			if speed > 0.0:
				reactions.append(r)
				reaction_speeds.append(speed)
				reaction_leftovers.append(0.0)

func _query_database() -> Array:
	return []
