extends Node2D

func _ready():
	pass # Replace with function body.

func _on_DaisyChain_setup_complete(v: Viewport):
	(material as ShaderMaterial).set_shader_param("exposure", v.get_texture())
