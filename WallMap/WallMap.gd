extends TileMap

export(int, 0, 10) var tile_expand: int = 1
export(float, 0, 10.0) var tile_scale: float = 1.0
export(Texture) var wall_texture: Texture = null

onready var child = $UpperWallMap

func _ready():
	child.upscale = tile_expand * 2 + 1
	child.tex = wall_texture
	child.scale = Vector2(tile_scale, tile_scale)
	child.copy_map()
