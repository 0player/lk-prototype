extends TileMap

onready var parent : TileMap = get_parent()
var upscale: int = 1
var screen_center: Vector2
var tex: Texture = null

var lines: PoolIntArray = []

const COLOR = Color.red

func _ready():
	copy_settings()

func _process(delta):
	var viewport_center := get_viewport().size * 0.5
	var screen_center_loc := parent.make_canvas_position_local(viewport_center)
	var new_position := screen_center_loc - transform.basis_xform(upscale * screen_center_loc)
	if !new_position.is_equal_approx(position):
		self.screen_center = upscale * screen_center_loc
		position = new_position
		update()
	
func _draw():
	var xform := transform.affine_inverse()
	var primitives: Array = []
	var uv_global: PoolVector2Array = [Vector2(0, 0), Vector2(1, 0), Vector2(1, 1), Vector2(0, 1)]
	if tex is AtlasTexture:
		var size := (tex as AtlasTexture).atlas.get_size()
		var region := (tex as AtlasTexture).region
		var xf := Transform2D(Vector2(region.size.x, 0), Vector2(0, region.size.y), region.position)
		xf = xf.scaled(Vector2(1.0 / size.x, 1.0 / size.y));
		for i in range(uv_global.size()):
			uv_global[i] = xf * uv_global[i]
	for lineidx in range(0, lines.size(), 4):
		var coords: PoolVector2Array = []
		var x1 := lines[lineidx]
		var y1 := lines[lineidx + 1]
		var x2 := lines[lineidx + 2]
		var y2 := lines[lineidx + 3]
		coords.resize(4)
		coords[0] = map_to_world(Vector2(x1, y1))
		coords[1] = map_to_world(Vector2(x2, y2))
		coords[2] = xform * parent.map_to_world(Vector2(parent_coord(x2), parent_coord(y2)))
		coords[3] = xform * parent.map_to_world(Vector2(parent_coord(x1), parent_coord(y1)))
		
		if triangle_area2(coords) <= 0:
			continue
			
		if coords[2] == coords[3]:
			coords.resize(3)
		
		primitives.append(coords)
	
	primitives.sort_custom(self, "distance_gt")
	
	for coords in primitives:
		
		var area := triangle_area2(coords)
		var height : float = area / coords[0].distance_to(coords[1]) / cell_size.x
		var cosine := upscale / sqrt(height * height + upscale * upscale)
		
		var color := Color(cosine, cosine, cosine, 1.0)
		
		var colors : PoolColorArray = [color, color, color, color]
		var uvs: PoolVector2Array = PoolVector2Array(uv_global)
		# hacky but oh well. If left side of the triangle is aligned w boundary,
		# the tip should align to the same boundary.
		if coords.size() == 3:
			var line_start := world_to_map(coords[0])
			if int(line_start.x) % upscale == 0 or int(line_start.y) % upscale == 0:
				uvs.remove(2)
			else:
				uvs.remove(3)
		
		colors.resize(coords.size())
		
		draw_primitive(coords, colors, uvs, tex, 1.0, null)

func parent_coord(coord: int) -> int:
	return (coord + upscale - upscale / 2 - 1) / upscale
	
static func triangle_area2(coords: PoolVector2Array) -> float:
	return (coords[2] - coords[1]).cross(coords[0] - coords[1])

func distancesq_to_center(a: PoolVector2Array) -> float:
	var res := screen_center.distance_squared_to(a[2])
	if a.size() == 4:
		res = min(res, screen_center.distance_squared_to(a[3]))
	return res
	

func distance_gt(a: PoolVector2Array, b: PoolVector2Array) -> bool:
	return distancesq_to_center(a) > distancesq_to_center(b)

func _on_WallMap_settings_changed():
	if is_inside_tree():
		copy_settings()

func copy_settings():
	tile_set = parent.tile_set
	cell_size = parent.cell_size
	# modes are not currently supported, sadly

func copy_map():
	clear()
	lines.resize(0)
	var center_offset: int = upscale / 2
	
	for pos in parent.get_used_cells():
		var x: int = int(pos.x)
		var y: int = int(pos.y)
		var cell_id := parent.get_cell(x, y)
		set_cell(upscale * x + center_offset, upscale * y + center_offset, cell_id)
		
		var up: bool = parent.get_cell(x, y - 1) != INVALID_CELL
		var down: bool = parent.get_cell(x, y + 1) != INVALID_CELL
		var left: bool = parent.get_cell(x - 1, y) != INVALID_CELL
		var right: bool = parent.get_cell(x + 1, y) != INVALID_CELL
		
		var x1 := x * upscale
		var x2 := x * upscale + center_offset
		var x3 := x * upscale + center_offset + 1
		var x4 := x * upscale + upscale
		
		var y1 := y * upscale
		var y2 := y * upscale + center_offset
		var y3 := y * upscale + center_offset + 1
		var y4 := y * upscale + upscale
		
		if parent.get_cell(x - 1, y - 1) != INVALID_CELL and up and left:
			set_cell_region(x1, y1, x2, y2, cell_id)
		else:
			if up:
				add_line(x2, y1, x2, y2)
			if left:
				add_line(x2, y2, x1, y2)
		if up:
			set_cell_region(x2, y1, x3, y2, cell_id)
		else:
			add_line(x3, y2, x2, y2)
		if parent.get_cell(x + 1, y - 1) != INVALID_CELL and up and right:
			set_cell_region(x3, y1, x4, y2, cell_id)
		else:
			if up:
				add_line(x3, y2, x3, y1)
			if right:
				add_line(x4, y2, x3, y2)
		if left:
			set_cell_region(x1, y2, x2, y3, cell_id)
		else:
			add_line(x2, y2, x2, y3)
		if right:
			set_cell_region(x3, y2, x4, y3, cell_id)
		else:
			add_line(x3, y3, x3, y2)
		if parent.get_cell(x - 1, y + 1) != INVALID_CELL and down and left:
			set_cell_region(x1, y3, x2, y4, cell_id)
		else:
			if down:
				add_line(x2, y3, x2, y4)
			if left:
				add_line(x1, y3, x2, y3)
		if down:
			set_cell_region(x2, y3, x3, y4, cell_id)
		else:
			add_line(x2, y3, x3, y3)
		if parent.get_cell(x + 1, y + 1) != INVALID_CELL and down and right:
			set_cell_region(x3, y3, x4, y4, cell_id)
		else:
			if down:
				add_line(x3, y4, x3, y3)
			if right:
				add_line(x3, y3, x4, y3)
	
	update_bitmask_region()
	
func set_cell_region(x_low: int, y_low: int, x_high: int, y_high: int, cell_id: int):
	for x in range(x_low, x_high):
		for y in range(y_low, y_high):
			set_cell(x, y, cell_id)

func add_line(xbeg: int, ybeg: int, xend: int, yend: int):
	lines.append(xbeg)
	lines.append(ybeg)
	lines.append(xend)
	lines.append(yend)
