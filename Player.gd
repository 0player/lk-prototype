extends KinematicBody2D

export var map_path : NodePath
export var tile_seconds := 0.5
onready var map : Map = get_node(map_path)
onready var tile_size := map.cell_size

var POSITION := @":position"
var path := PoolVector2Array([])
var speedy := false

# Called when the node enters the scene tree for the first time.
func _ready():
	var maprect := map.get_used_rect()
	var cam := $Camera2D
	var topleft := map.map_to_world(maprect.position)
	var bottomright := map.map_to_world(maprect.end)
	var viewsize := get_viewport_rect().size
	cam.limit_left = topleft.x - viewsize.x / 2
	cam.limit_top = topleft.y - viewsize.y / 2
	cam.limit_right = bottomright.x + viewsize.x / 2
	cam.limit_bottom = bottomright.y + viewsize.y / 2
	
	print(cam.limit_left)
	print(cam.limit_right)
	print(cam.limit_top)
	print(cam.limit_bottom)
	print(cam.global_position)
	print(cam.get_camera_screen_center())

onready var tween = $Tween
onready var interaction = $Interaction

func _unhandled_input(event):
	var PlayerPosition : Vector2
	var PlayerDirection : Vector2
	if path.empty():
		PlayerPosition = position
	else:
		PlayerPosition = path[0]
	PlayerDirection = Vector2(
		Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"),
		Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	).normalized()
	
	if PlayerDirection != Vector2.ZERO:
		var Velocity := PlayerDirection.dot(tile_size) * PlayerDirection.abs()
		var coll := test_move(Transform2D(Vector2.RIGHT, Vector2.DOWN, PlayerPosition), Velocity)
		if !coll:
			print("path = ", path, "from = ", PlayerPosition, " moving to ", PlayerPosition + Velocity)
			append_path([PlayerPosition + Velocity])
		get_tree().set_input_as_handled()
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			var space_state = get_world_2d().direct_space_state
			var result = space_state.intersect_point(event.position, 1, [], 0x2)
			for object in result:
				var shape = object["collider"]
				if interaction.overlaps_body(shape):
					print(object)
			get_tree().set_input_as_handled()
		if event.button_index == BUTTON_RIGHT and event.pressed:
			var p := map.get_path_between(PlayerPosition, event.position)
			if p.size() > 1:
				p.remove(0)
				append_path(p)
			get_tree().set_input_as_handled()

func append_path(p: PoolVector2Array):
	if path.empty():
		path.append_array(p)
		var speed := tile_seconds
		if !speedy: speedy = p.size() > 1
		if speedy: speed /= 2
		var easing := Tween.EASE_IN_OUT
		if p.size() > 1:
			easing = Tween.EASE_IN
		print("tweening from ", position, " to ", p[0])
		tween.interpolate_property(self, "position", position, p[0],
			speed, Tween.TRANS_CUBIC, easing)
		tween.start()
	else:
		path.resize(1)
		path.append_array(p)
		if !speedy:
			speedy = true
			var current_time = tween.tell()
			tween.remove(self, "position")
			print("retweening from ", position, " to ", path[0])
			tween.interpolate_property(self, "position", position, path[0],
				(tile_seconds - current_time) / 2, Tween.TRANS_CUBIC, Tween.EASE_IN)
			tween.start()
			
		

func _on_Tween_tween_completed(object, key):
	if object == self and key == POSITION:
		print("popping path = ", path, " at ", position)
		assert(!path.empty() && path[0] == position)
		path.remove(0)
		if !path.empty():
			var speed = tile_seconds
			if speedy:
				speed /= 2
			var transition := Tween.TRANS_CUBIC
			if path.size() > 1:
				transition = Tween.TRANS_LINEAR
				speed /= 3
			print("contweening from ", position, " to ", path[0])
			#tween.interpolate_property(self, "position", position, path[0],
			#	speed, transition, Tween.EASE_OUT)
			tween.call_deferred("interpolate_property", self, "position", position, path[0],
				speed, transition, Tween.EASE_OUT)
			tween.start()
		else:
			speedy = false
